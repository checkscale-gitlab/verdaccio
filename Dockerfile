FROM verdaccio/verdaccio:4

USER root

ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python

ENV NODE_ENV=production

RUN mkdir -p /opt/verdaccio/.cache/node-gyp
RUN chmod +xrw -R /opt/verdaccio/.cache/node-gyp

RUN npm install --global verdaccio-minio verdaccio-badges verdaccio-gitlab-ci verdaccio-gitlab verdaccio-aws-s3-storage

RUN ln -s /usr/local/lib/node_modules/verdaccio-minio /verdaccio/plugins/verdaccio-minio && \
    ln -s /usr/local/lib/node_modules/verdaccio-badges /verdaccio/plugins/verdaccio-badges && \
    ln -s /usr/local/lib/node_modules/verdaccio-gitlab-ci /verdaccio/plugins/verdaccio-gitlab-ci && \
    ln -s /usr/local/lib/node_modules/verdaccio-gitlab /verdaccio/plugins/verdaccio-gitlab && \
    ln -s /usr/local/lib/node_modules/verdaccio-aws-s3-storage /verdaccio/plugins/verdaccio-aws-s3-storage

RUN chown -R 10001 /verdaccio/plugins

USER verdaccio
